package top.yqingyu.qyrpc.autoconfigure;

import com.alibaba.fastjson2.JSON;
import jakarta.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import top.yqingyu.common.utils.UUIDUtil;
import top.yqingyu.qymsg.Dict;
import top.yqingyu.rpc.consumer.Consumer;
import top.yqingyu.rpc.consumer.ConsumerHolderContext;
import top.yqingyu.rpc.consumer.MethodExecuteInterceptor;
import top.yqingyu.rpc.producer.Producer;
import top.yqingyu.rpc.producer.QyRpcInterceptorChain;
import top.yqingyu.rpc.producer.ServerExceptionHandler;

import java.util.Map;
import java.util.Set;

@Import({
//        ConsumerBeanProxyFactory.class, 废弃。 不再使用QyRpcConsumer注入，使用 Spring auto
        ProducerBeanRegister.class})
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(QyRpcProperties.class)
public class QyRpcAutoConfiguration implements InitializingBean {
    public static final Logger logger = LoggerFactory.getLogger(QyRpcAutoConfiguration.class);
    @Resource
    private QyRpcProperties properties;
    @Resource
    ApplicationContext ctx;

    @Bean
    @ConditionalOnMissingBean
    public ServerExceptionHandler qyrpcExceptionHandler() {
        logger.info("qyrpc producer use inner ExceptionHandler");
        return new ServerExceptionHandler() {
        };
    }

    @Bean
    @ConditionalOnMissingBean
    public QyRpcInterceptorChain producerChain() {
        return new QyRpcInterceptorChain();
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnBean({ServerExceptionHandler.class, QyRpcInterceptorChain.class})
    @ConditionalOnProperty(prefix = Constants.prefix, name = "mode")
    public Producer qyrpcProducer(ServerExceptionHandler serverExceptionHandler,QyRpcInterceptorChain chain) throws Exception {
        ProducerConfig config = properties.getProducer();
        if (config == null) {
            config = new ProducerConfig();
        }
        Producer build = Producer.Builder.newBuilder()
                .port(config.port)
                .bodyLengthMax(config.bodyLengthMax)
                .radix(config.radix)
                .threadName(config.threadName)
                .exceptionHandler(serverExceptionHandler)
                .interceptorChain(chain)
                .pool(config.pool)
                .serverName(config.serverName)
                .build();

        try {
            switch (properties.getMode()) {
                case PRODUCER, BOTH -> {
                    logger.info("Initialize autoConfigure qyrpc Producer");
                    build.start();
                    logger.info("Initialized qyrpc Producer");
                }
            }
        } catch (Throwable t) {
            build.shutdown();
            throw t;
        }

        return build;
    }

    @Bean
    @ConditionalOnBean({ConsumerBeanConfigure.class})
    @ConditionalOnMissingBean
    public MethodExecuteInterceptor qyrpcMethodExecuteInterceptor() {
        logger.info("qyrpc consumer use inner MethodExecuteInterceptor");
        return new MethodExecuteInterceptor() {
        };
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnBean({MethodExecuteInterceptor.class})
    @ConditionalOnProperty(prefix = Constants.prefix, name = "mode")
    public ConsumerHolderContext qyrpcConsumerHolderContext() throws Exception {
        ConsumerHolderContext context = new ConsumerHolderContext();
        try {
            String uuid = UUIDUtil.randomUUID().toString2();
            switch (properties.getMode()) {
                case CONSUMER, BOTH -> {
                    logger.info("Initialize autoConfigure qyrpc Consumer");
                    MethodExecuteInterceptor proxyMethodInterceptor = ctx.getBean(MethodExecuteInterceptor.class);
                    context.setMethodExecuteInterceptor(proxyMethodInterceptor);
                    Map<String, ConsumerConfig> consumerConfigMap = properties.getConsumer();
                    Set<String> consumerConfigKey = consumerConfigMap.keySet();
                    for (String serverName : consumerConfigKey) {
                        ConsumerConfig consumerConfig = consumerConfigMap.get(serverName);
                        for (String url : consumerConfig.url) {
                            String host;
                            int port;
                            if (!url.startsWith("qyrpc://")) {
                                throw new IllegalArgumentException("[{}] An incorrect url exists in the configuration file: {}，plz check it. e.g. qyrpc://host:port", serverName, url);
                            }
                            url = url.replaceFirst("qyrpc://", "").trim();
                            String[] split = url.split(":");
                            if (split.length != 2) {
                                throw new IllegalArgumentException("[{}] An incorrect url exists in the configuration file: {}，plz check it. e.g. qyrpc://host:port", serverName, url);
                            }
                            try {
                                host = split[0].trim();
                                port = Integer.parseInt(split[1].trim());
                            } catch (Exception e) {
                                throw new IllegalArgumentException("[{}] An incorrect url exists in the configuration file: {}，plz check it. e.g. qyrpc://host:port", serverName, url);
                            }
                            top.yqingyu.rpc.consumer.conf.ConsumerConfig config = new top.yqingyu.rpc.consumer.conf.ConsumerConfig();
                            config.setName(serverName);
                            config.setThreadName(consumerConfig.threadName);
                            config.setPoolMax(consumerConfig.poolMax);
                            config.setPoolMin(consumerConfig.poolMin);
                            config.setRadix(consumerConfig.radix);
                            config.setClearTime(consumerConfig.clearTime);
                            config.setBodyLengthMax(consumerConfig.bodyLengthMax);
                            config.setHost(host);
                            config.setPort(port);
                            config.setProxyMode(consumerConfig.proxyMode);
                            // 开启注册中心。
                            config.setEnableRegister(false);
                            if (consumerConfig.id.length() != Dict.CLIENT_ID_LENGTH) {
                                logger.warn("[{}] Because the client ID is not configured or the id of the value is not equal to 32 bits, 32-bit unsigned UUID will be used {}", serverName, uuid);
                            }
                            Consumer.create(config, context);
                        }
                        logger.info("Initialized qyrpc Consumer [{}]", serverName);
                    }
                    logger.info("Initialized all qyrpc Consumer");
                }
            }
            return context;
        } catch (Throwable t) {
            context.shutdown();
            throw t;
        }
    }

    @Override
    public void afterPropertiesSet() {
//        ConsumerBeanProxyFactory bean = ctx.getBean(ConsumerBeanProxyFactory.class);
//        bean.properties = properties;
    }
}
